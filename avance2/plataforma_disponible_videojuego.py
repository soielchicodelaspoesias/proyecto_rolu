import tkinter as tk
from tkinter import ttk

class plataformadisponible:
    def __init__(self, root, db):
        self.db = db
        self.data = []

        self.root = tk.Toplevel()
        self.root.geometry('600x400')
        self.root.title("plataformadisponible")
        self.root.resizable(width=0, height=0)

        # toplevel modal

        self.root.transient(root)

        #
        self.__config_treeview_plataformadisponible()
        self.__config_buttons_plataformadisponible()

    def __config_treeview_plataformadisponible(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3"))
        self.treeview.heading("#0", text = "Id_videojuego")
        self.treeview.heading("#1", text = "Videojuego")
        self.treeview.heading("#2", text = "Id_plataforma")
        self.treeview.heading("#3", text = "Plataforma")
        self.treeview.column("#0", minwidth = 150, width = 150, stretch = False)
        self.treeview.column("#1", minwidth = 150, width = 150, stretch = False)
        self.treeview.column("#2", minwidth = 150, width = 150, stretch = False)
        self.treeview.column("#3", minwidth = 150, width = 150, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 700)
        self.llenar_treeview_plataformadisponible()
        self.root.after(0, self.llenar_treeview_plataformadisponible)

    def __config_buttons_plataformadisponible(self):
        tk.Button(self.root, text="Insertar",
            command = self.__insertar_plataformadisponible).place(x = 0, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Modificar",
            command = self.__modificar_plataformadisponible).place(x = 200, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Eliminar",
            command = self.__eliminar_plataformadisponible).place(x = 400, y = 350, width = 200, height = 50)

    def llenar_treeview_plataformadisponible(self):
        sql = """select videojuego.id_videojuego, videojuego.nombre, plataforma.id_plataforma, plataforma.nombre_plataforma from plataforma_disponible_videojuego join videojuego on plataforma_disponible_videojuego.videojuego_id_videojuego = videojuego.id_videojuego join plataforma on plataforma_disponible_videojuego.plataforma_id_plataforma = plataforma.id_plataforma"""
        data = self.db.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], i[2], i[3]))
            self.data = data

    def __insertar_plataformadisponible(self):
        insertar_plataformadisponible(self.db, self)

    def __modificar_plataformadisponible(self):
        if(self.treeview.focus() != ""):
            sql = """select * from plataforma_disponible_videojuego where videojuego_id_videojuego = %(videojuego_id_videojuego)s"""
            row_data = self.db.run_select_filter(sql, {"videojuego_id_videojuego": self.treeview.focus()})
            modificar_plataformadisponible(self.db, self, row_data)

    def __eliminar_plataformadisponible(self):
        sql = "delete from plataforma_disponible_videojuego where plataforma_id_plataforma = %(plataforma_id_plataforma)s;"
        self.db.run_sql(sql, {"plataforma_id_plataforma": self.treeview.focus()})
        self.llenar_treeview_plataformadisponible()

class insertar_plataformadisponible:
    def __init__(self, db, padre):
        self.padre = padre
        self.db = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Insertar Plataforma disponible videojuego")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):
        tk.Label(self.insert_datos, text = "Videojuego: ").place(x = 10, y = 10, width = 80, height = 20)
        tk.Label(self.insert_datos, text = "Plataforma: ").place(x = 10, y = 40, width = 80, height = 20)

    def __config_entry(self):
        self.combovideojuego = ttk.Combobox(self.insert_datos)
        self.combovideojuego.place(x = 110, y = 10, width = 80, height= 20)
        self.combovideojuego["values"], self.ids = self.__fill_combovideojuego()

        self.comboplataforma = ttk.Combobox(self.insert_datos)
        self.comboplataforma.place(x = 110, y = 40, width = 80, height= 20)
        self.comboplataforma["values"], self.ids = self.__fill_comboplataforma()

    def __config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =100, width = 200, height = 20)

    def __fill_combovideojuego(self):
        sql = "select * from videojuego"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __fill_comboplataforma(self):
        sql = "select * from plataforma"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __insertar(self): #Insercion en la base de datos.
        sql = """insert plataforma_disponible_videojuego (plataforma_id_plataforma, videojuego_id_videojuego)
            values (%(plataforma_id_plataforma)s, %(videojuego_id_videojuego)s);"""
        self.db.run_sql(sql, {"plataforma_id_plataforma": self.ids[self.comboplataforma.current()],
       "videojuego_id_videojuego": self.ids[self.combovideojuego.current()]})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_plataformadisponible()

class modificar_plataformadisponible:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.db = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Modificar")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self): #Labels
        tk.Label(self.insert_datos, text = "Videojuego: ").place(x = 10, y = 10, width = 80, height = 20)
        tk.Label(self.insert_datos, text = "Plataforma: ").place(x = 10, y = 40, width = 80, height = 20)

    def config_entry(self):#Se configuran los inputs
        self.combovideojuego = ttk.Combobox(self.insert_datos)
        self.combovideojuego.place(x = 110, y = 10, width = 80, height= 20)
        self.combovideojuego["values"], self.ids = self.fill_combovideojuego()
        self.combovideojuego.insert(0, self.row_data[0])

        self.comboplataforma = ttk.Combobox(self.insert_datos)
        self.comboplataforma.place(x = 110, y = 40, width = 80, height= 20)
        self.comboplataforma["values"], self.ids = self.fill_comboplataforma()
        self.comboplataforma.insert(0, self.row_data[0])


    def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
        command = self.modificar).place(x=0, y =100, width = 200, height = 20)

    def modificar(self): #Insercion en la base de datos.
        sql = """update plataforma_disponible_videojuego set
        plataforma_id_plataforma = %(plataforma_id_plataforma)s,
        where videojuego_id_videojuego = %(videojuego_id_videojuego)s"""
        print("id_plataforma", self.ids[self.comboplataforma.current()])
        print("id_videojuego", self.ids[self.combovideojuego.current()])

        self.db.run_sql(sql, {"plataforma_id_plataforma": self.ids[self.comboplataforma.current()],
        "videojuego_id_videojuego": self.ids[self.combovideojuego.current()]})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_plataformadisponible()

    def fill_combovideojuego(self): #
        sql = "select * from videojuego"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def fill_comboplataforma(self): #
        sql = "select * from plataforma"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]
