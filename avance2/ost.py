import tkinter as tk
from tkinter import ttk
from tkcalendar import DateEntry
from tkinter import Button
from discografia import discografia

class ost:
    def __init__(self, root, db):
        self.db = db
        self.data = []

        #Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('600x400')
        self.root.title("OSTs")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.__config_treeview_ost()
        self.__config_buttons_ost()


    def __config_treeview_ost(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "OST")
        self.treeview.heading("#2", text = "Fecha")
        self.treeview.heading("#3", text = "Discografia")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 200, width = 200, stretch = False)
        self.treeview.column("#2", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#3", minwidth = 200, width = 200, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 700)
        self.llenar_treeview_ost()
        self.root.after(0, self.llenar_treeview_ost)

    def __config_buttons_ost(self):
        tk.Button(self.root, text="Insertar ost",
            command = self.__insertar_ost).place(x = 0, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Modificar ost",
            command = self.__modificar_ost).place(x = 200, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Eliminar ost",
            command = self.__eliminar_ost).place(x = 400, y = 350, width = 200, height = 50)

    def llenar_treeview_ost(self):
        sql = """select id_ost, nombre, fecha, discografia.nombre_discografia from ost join discografia
        on ost.discografia_id_discografia = discografia.id_discografia;"""
        data = self.db.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = str(i[0]),
                    values = (str(i[1]), str(i[2]), str(i[3])), iid = i[0],tags = "rojo")
            self.data = data

    def __insertar_ost(self):
        insertar_ost(self.db, self)

    def __modificar_ost(self):
        if(self.treeview.focus() != ""):
            sql = """select ost.id_ost, fecha, nombre, discografia.nombre_discografia from ost join discografia
            on ost.discografia_id_discografia = discografia.id_discografia where id_ost = %(id_ost)s"""

            row_data = self.db.run_select_filter(sql, {"id_ost": self.treeview.focus()})[0]
            modificar_ost(self.db, self, row_data)

    def __eliminar_ost(self):
        sql = "delete from ost where id_ost = %(id_ost)s"
        self.db.run_sql(sql, {"id_ost": self.treeview.focus()})
        self.llenar_treeview_ost()

class insertar_ost:
    def __init__(self, db, padre):
        self.padre = padre
        self.db = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Insertar ost")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):
        tk.Label(self.insert_datos, text = "Fecha: ").place(x = 10, y = 10, width = 80, height = 20)
        tk.Label(self.insert_datos, text = "OST: ").place(x = 10, y = 40, width = 80, height = 20)
        tk.Label(self.insert_datos, text = "Discografia: ").place(x = 10, y = 70, width = 80, height = 20)

    def __config_entry(self):

        self.combo1 = DateEntry(self.insert_datos,selectmode='day')
        self.combo1.place(x = 110, y = 10, width = 80, height= 20)
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 110, y = 40, width = 80, height = 20)
        self.combo = ttk.Combobox(self.insert_datos)
        self.combo.place(x = 110, y = 70, width = 80, height= 20)
        self.combo["values"], self.ids = self.__fill_combo()

    def __config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =100, width = 200, height = 20)

    def __fill_combo(self):
        sql = "select id_discografia, nombre_discografia from discografia"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __insertar(self): #Insercion en la base de datos.
        sql = """insert ost (discografia_id_discografia, fecha, nombre)
            values (%(id_discografia)s, %(fecha)s, %(nombre)s)"""
        self.db.run_sql(sql, {"id_discografia": self.ids[self.combo.current()],
            "fecha": self.combo1.get_date(), "nombre": self.entry_nombre.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_ost()

class modificar_ost:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.db = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Modificar ost")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self): #Labels
        tk.Label(self.insert_datos, text = "Fecha: ").place(x = 10, y = 10, width = 80, height = 20)
        tk.Label(self.insert_datos, text = "OST: ").place(x = 10, y = 40, width = 80, height = 20)
        tk.Label(self.insert_datos, text = "Discografia: ").place(x = 10, y = 70, width = 80, height = 20)

    def config_entry(self):#Se configuran los inputs
        self.combo1 = DateEntry(self.insert_datos,selectmode='day')
        self.combo1.place(x = 110, y = 10, width = 80, height = 20)
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 110, y = 40, width = 80, height = 20)
        self.combo = ttk.Combobox(self.insert_datos)
        self.combo.place(x = 110, y = 70, width = 80, height= 20)
        self.combo["values"], self.ids = self.fill_combo()
        self.combo1.insert(0, self.row_data[1])
        self.entry_nombre.insert(0, self.row_data[2])
        self.combo.insert(0, self.row_data[3])

    def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.modificar).place(x=0, y =100, width = 200, height = 20)

    def modificar(self): #Insercion en la base de datos.
        sql = """update ost set fecha = %(fecha)s, nombre = %(nombre)s,
            discografia_id_discografia = %(id_discografia)s where id_ost = %(id_ost)s"""
        self.db.run_sql(sql, {"fecha": self.combo1.get_date(),
            "nombre": self.entry_nombre.get(), "id_discografia": self.ids[self.combo.current()],
                "id_ost": int(self.row_data[0])})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_ost()

    def fill_combo(self): #
        sql = "select id_discografia, nombre_discografia from discografia"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]
