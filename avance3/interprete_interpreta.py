import tkinter as tk
from tkinter import ttk

class interpreta:
    def __init__(self, root, db):
        self.db = db
        self.data = []

        self.root = tk.Toplevel()
        self.root.geometry('600x400')
        self.root.title("interpreta")
        self.root.resizable(width=0, height=0)

        # toplevel modal

        self.root.transient(root)

        #
        self.__config_treeview_interpreta()
        self.__config_buttons_interpreta()

    def __config_treeview_interpreta(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#0", "#1", "#2", "#3", "#4"))
        self.treeview.heading("#0", text = "")
        self.treeview.heading("#1", text = "Id interprete")
        self.treeview.heading("#2", text = "Interprete")
        self.treeview.heading("#3", text = "Id OST")
        self.treeview.heading("#4", text = "OST")
        self.treeview.column("#0", minwidth = 0, width = 0, stretch = False)
        self.treeview.column("#1", minwidth = 150, width = 150, stretch = False)
        self.treeview.column("#2", minwidth = 150, width = 150, stretch = False)
        self.treeview.column("#3", minwidth = 150, width = 150, stretch = False)
        self.treeview.column("#4", minwidth = 150, width = 150, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 700)
        self.llenar_treeview_interpreta()
        self.root.after(0, self.llenar_treeview_interpreta)

    def __config_buttons_interpreta(self):
        tk.Button(self.root, text="Insertar",
            command = self.__insertar_interpreta).place(x = 0, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Modificar",
            command = self.__modificar_interpreta).place(x = 200, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Eliminar",
            command = self.__eliminar_interpreta).place(x = 400, y = 350, width = 200, height = 50)

    def llenar_treeview_interpreta(self):
        sql = """select id_interprete, nombre_artista, ost.id_ost, ost.nombre from
         interprete join interprete_interpreta_ost on
         interprete_interpreta_ost.interprete_id_interprete = interprete.id_interprete
         join ost on
         ost.id_ost = interprete_interpreta_ost.ost_id_ost"""
        data = self.db.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", iid = [i[0], i[2]], values = (i[0:4]))
            self.data = data

    def __insertar_interpreta(self):
        insertar_interpreta(self.db, self)

    def __modificar_interpreta(self):
        if(self.treeview.focus() != ""):
            # Lista que contiene columnas de todo el registro seleccionado
            item = (self.treeview.item(self.treeview.focus()))["values"]
            print("ITEM", item)
            # Se obtienen claves primarias
            self.claves = [item[0], item[2]]
            print("CLAVES", self.claves)

            sql = """select * from interprete_interpreta_ost where
            interprete_id_interprete = %(id_interprete)s and
            ost_id_ost = %(id_ost)s"""
            row_data = self.db.run_select_filter(sql, {"id_interprete": self.claves[0], "id_ost": self.claves[1]})[0]
            modificar_interpreta(self.db, self, row_data)

    def __eliminar_interpreta(self):
        item = (self.treeview.item(self.treeview.focus()))["values"]
        self.claves = [item[0], item[2]]
        print("CLAVES", self.claves)
        sql = """delete from interprete_interpreta_ost where
        interprete_id_interprete = %(id_interprete)s and
        ost_id_ost = %(id_ost)s"""
        self.db.run_sql(sql, {"id_interprete": self.claves[0], "id_ost": self.claves[1]})
        self.llenar_treeview_interpreta()

class insertar_interpreta:
    def __init__(self, db, padre):
        self.padre = padre
        self.db = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("interprete interpreta")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):
        tk.Label(self.insert_datos, text = "Interprete: ").place(x = 10, y = 10, width = 80, height = 20)
        tk.Label(self.insert_datos, text = "OST: ").place(x = 10, y = 40, width = 80, height = 20)

    def __config_entry(self):
        self.combointerprete = ttk.Combobox(self.insert_datos)
        self.combointerprete.place(x = 110, y = 10, width = 80, height= 20)
        self.combointerprete["values"], self.ids = self.__fill_combointerprete()
        self.comboOST = ttk.Combobox(self.insert_datos)
        self.comboOST.place(x = 110, y = 40, width = 80, height= 20)
        self.comboOST["values"], self.ids = self.__fill_comboOST()

    def __config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =100, width = 200, height = 20)

    def __fill_combointerprete(self):
        sql = "select id_interprete, nombre_artista from interprete"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __fill_comboOST(self):
        sql = "select id_ost, nombre from ost"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __insertar(self): #Insercion en la base de datos.
        sql = """insert interprete_interpreta_ost (interprete_id_interprete,ost_id_ost)
            values (%(interprete_id_interprete)s, %(ost_id_ost)s);"""
        self.db.run_sql(sql, {"ost_id_ost": self.ids[self.comboOST.current()],
       "interprete_id_interprete": self.ids[self.combointerprete.current()]})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_interpreta()

class modificar_interpreta:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.db = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Modificar")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self): #Labels
        tk.Label(self.insert_datos, text = "Interprete: ").place(x = 10, y = 10, width = 80, height = 20)
        tk.Label(self.insert_datos, text = "OST: ").place(x = 10, y = 40, width = 80, height = 20)


    def config_entry(self):#Se configuran los inputs
        self.combointerprete = ttk.Combobox(self.insert_datos)
        self.combointerprete.place(x = 110, y = 10, width = 80, height= 20)
        self.combointerprete["values"], self.ids_inter = self.fill_combointerprete()
        self.comboOST = ttk.Combobox(self.insert_datos)
        self.comboOST.place(x = 110, y = 40, width = 80, height= 20)
        self.comboOST["values"], self.ids_ost = self.fill_comboOST()

        self.idinterprete = self.row_data[0]
        self.idOST = self.row_data[1]

    def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.modificar).place(x=0, y =100, width = 200, height = 20)

    def modificar(self): #Insercion en la base de datos.
        sql = """update interprete_interpreta_ost set
        interprete_id_interprete = %(id_interprete)s,
        ost_id_ost = %(id_ost)s
        where interprete_id_interprete = %(interprete_id_interprete)s and
        ost_id_ost = %(ost_id_ost)s"""

        self.db.run_sql(sql, {"id_interprete": self.ids_inter[self.combointerprete.current()],
                     "id_ost": self.ids_ost[self.comboOST.current()],
                     "interprete_id_interprete": self.idinterprete,
                     "ost_id_ost": self.idOST})
        self.padre.llenar_treeview_interpreta()


    def fill_combointerprete(self):
        sql = "select id_interprete, nombre_artista from interprete"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def fill_comboOST(self):
        sql = "select id_ost, nombre from ost"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]
