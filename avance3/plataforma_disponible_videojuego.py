import tkinter as tk
from tkinter import ttk

class plataformadisponible:
    def __init__(self, root, db):
        self.db = db
        self.data = []

        self.root = tk.Toplevel()
        self.root.geometry('600x400')
        self.root.title("plataformadisponible")
        self.root.resizable(width=0, height=0)

        # toplevel modal

        self.root.transient(root)

        #
        self.__config_treeview_plataformadisponible()
        self.__config_buttons_plataformadisponible()

    def __config_treeview_plataformadisponible(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#0", "#1", "#2", "#3", "#4"))
        self.treeview.heading("#0", text = " ")
        self.treeview.heading("#1", text = "Id plataforma")
        self.treeview.heading("#2", text = "Plataforma")
        self.treeview.heading("#3", text = "Id videojuego")
        self.treeview.heading("#4", text = "Videojuego")
        self.treeview.column("#0", minwidth = 0, width = 0, stretch = False)
        self.treeview.column("#1", minwidth = 150, width = 150, stretch = False)
        self.treeview.column("#2", minwidth = 150, width = 150, stretch = False)
        self.treeview.column("#3", minwidth = 150, width = 150, stretch = False)
        self.treeview.column("#4", minwidth = 150, width = 150, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 700)
        self.llenar_treeview_plataformadisponible()
        self.root.after(0, self.llenar_treeview_plataformadisponible)

    def __config_buttons_plataformadisponible(self):
        tk.Button(self.root, text="Insertar",
            command = self.__insertar_plataformadisponible).place(x = 0, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Modificar",
            command = self.__modificar_plataformadisponible).place(x = 200, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Eliminar",
            command = self.__eliminar_plataformadisponible).place(x = 400, y = 350, width = 200, height = 50)



    def llenar_treeview_plataformadisponible(self):
        sql = """select plataforma.id_plataforma, plataforma.nombre_plataforma, videojuego.id_videojuego, videojuego.nombre
        from plataforma_disponible_videojuego join videojuego on
        plataforma_disponible_videojuego.videojuego_id_videojuego = videojuego.id_videojuego
        join plataforma on
        plataforma_disponible_videojuego.plataforma_id_plataforma = plataforma.id_plataforma"""
        data = self.db.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", iid = [i[0], i[2]], values = (i[0:4]))
            self.data = data

    def __insertar_plataformadisponible(self):
        insertar_plataformadisponible(self.db, self)

    def __modificar_plataformadisponible(self):
        if(self.treeview.focus() != ""):
            # Lista que contiene columnas de todo el registro seleccionado
            item = (self.treeview.item(self.treeview.focus()))["values"]
            print("ITEM", item)
            # Se obtienen claves primarias
            self.claves = [item[0], item[2]]
            print("CLAVES", self.claves)

            sql = """select * from plataforma_disponible_videojuego where
            plataforma_id_plataforma = %(id_plat)s and videojuego_id_videojuego = %(id_vid)s;"""
            row_data = self.db.run_select_filter(sql, {"id_plat": self.claves[0], "id_vid": self.claves[1]})[0]
            modificar_plataformadisponible(self.db, self, row_data)

    def __eliminar_plataformadisponible(self):
        item = (self.treeview.item(self.treeview.focus()))["values"]
        self.claves = [item[0], item[2]]
        print("CLAVES", self.claves)
        sql = """delete from
        plataforma_disponible_videojuego where
        plataforma_id_plataforma = %(id_plataforma)s and
        videojuego_id_videojuego = %(id_videojuego)s"""
        row_data = self.db.run_select_filter(sql, {"id_plataforma": self.claves[0],
                                                   "id_videojuego": self.claves[1]})
        self.llenar_treeview_plataformadisponible()

class insertar_plataformadisponible:
    def __init__(self, db, padre):
        self.padre = padre
        self.db = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Insertar Plataforma disponible videojuego")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):
        tk.Label(self.insert_datos, text = "Videojuego: ").place(x = 10, y = 10, width = 80, height = 20)
        tk.Label(self.insert_datos, text = "Plataforma: ").place(x = 10, y = 40, width = 80, height = 20)

    def __config_entry(self):
        self.combovideojuego = ttk.Combobox(self.insert_datos)
        self.combovideojuego.place(x = 110, y = 10, width = 80, height= 20)
        self.combovideojuego["values"], self.ids = self.__fill_combovideojuego()
        self.comboplataforma = ttk.Combobox(self.insert_datos)
        self.comboplataforma.place(x = 110, y = 40, width = 80, height= 20)
        self.comboplataforma["values"], self.ids = self.__fill_comboplataforma()

    def __config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =100, width = 200, height = 20)

    def __fill_combovideojuego(self):
        sql = "select id_videojuego, nombre from videojuego"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __fill_comboplataforma(self):
        sql = "select id_plataforma, nombre_plataforma from plataforma"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __insertar(self): #Insercion en la base de datos.
        sql = """insert plataforma_disponible_videojuego (plataforma_id_plataforma, videojuego_id_videojuego)
            values (%(plataforma_id_plataforma)s, %(videojuego_id_videojuego)s);"""
        self.db.run_sql(sql, {"plataforma_id_plataforma": self.ids[self.comboplataforma.current()],
       "videojuego_id_videojuego": self.ids[self.combovideojuego.current()]})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_plataformadisponible()

class modificar_plataformadisponible:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.db = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Modificar")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self): #Labels
        tk.Label(self.insert_datos, text = "Videojuego: ").place(x = 10, y = 10, width = 80, height = 20)
        tk.Label(self.insert_datos, text = "Plataforma: ").place(x = 10, y = 40, width = 80, height = 20)

    def config_entry(self):#Se configuran los inputs
        self.combovideojuego = ttk.Combobox(self.insert_datos)
        self.combovideojuego.place(x = 110, y = 10, width = 80, height= 20)
        self.combovideojuego["values"], self.ids_vid = self.fill_combovideojuego()
        self.comboplataforma = ttk.Combobox(self.insert_datos)
        self.comboplataforma.place(x = 110, y = 40, width = 80, height= 20)
        self.comboplataforma["values"], self.ids_plat = self.fill_comboplataforma()

        # Se guardan valores actuales
        self.idplataforma = self.row_data[0]
        self.idvideojuego = self.row_data[1]

    def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.modificar).place(x=0, y =100, width = 200, height = 20)

    def fill_combovideojuego(self):
        sql = "select id_videojuego, nombre from videojuego"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def fill_comboplataforma(self):
        sql = "select id_plataforma, nombre_plataforma from plataforma"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def modificar(self): #Insercion en la base de datos.
        sql = """update plataforma_disponible_videojuego set
        plataforma_id_plataforma = %(plataforma)s,
        videojuego_id_videojuego = %(videojuego)s where
        plataforma_id_plataforma = %(id_plat)s and videojuego_id_videojuego = %(id_vid)s;"""

        self.db.run_sql(sql, {"plataforma": self.ids_plat[self.comboplataforma.current()],
         "videojuego": self.ids_vid[self.combovideojuego.current()], "id_plat": self.idplataforma,
         "id_vid": self.idvideojuego})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_plataformadisponible()
