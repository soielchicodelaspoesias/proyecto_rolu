import tkinter as tk
from tkinter import ttk


class videojuego_por_desarrolladora:
    def __init__(self, root, db):
        self.db = db
        self.data = []
        self.insert_datos = tk.Toplevel()
        self.__config_entry()
        #Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('750x600')
        self.root.title("Videojuego por desarrolladora")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.__config_treeview_videojuego_por_desarrolladora()
        self.__config_buttons_videojuego_por_desarrolladora()

    def __config_treeview_videojuego_por_desarrolladora(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3", "#4"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.heading("#2", text = "Fecha")
        self.treeview.heading("#3", text = "Nota")
        self.treeview.heading("#4", text = "Desarrolladora")
        self.treeview.column("#0", minwidth = 40, width = 40, stretch = False)
        self.treeview.column("#1", minwidth = 120, width = 120, stretch = False)
        self.treeview.column("#2", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#3", minwidth = 40, width = 40, stretch = False)
        self.treeview.column("#4", minwidth = 120, width = 120, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 600, width = 750)
        self.llenar_treeview_videojuego_por_desarrolladora()
        self.root.after(0, self.llenar_treeview_videojuego_por_desarrolladora)

    def __config_buttons_videojuego(self):
        tk.Label(self.insert_datos, text = "Nombre: ").place(x = 50, y = 350, width = 80, height = 30)
        tk.Button(self.root, text="Consultar",
        command = self.llenar_treeview_videojuego_por_desarrolladora).place(x = 200, y = 350, width = 200, height = 50)

    def __config_entry(self):
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 100, y = 350, width = 80, height = 20)

    def llenar_treeview_videojuego_por_desarrolladora(self):
        sql = """call videojuego_por_desarrolladora (%(nombre)s)"""

        data = self.db.run_sql(sql, {"nombre": self.entry_nombre.get()})

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], str(i[2]), i[3], i[4]))
            self.data = data
