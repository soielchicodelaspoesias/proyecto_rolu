-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema proyecto_rolu
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema proyecto_rolu
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `proyecto_rolu` ;
USE `proyecto_rolu` ;

-- -----------------------------------------------------
-- Table `proyecto_rolu`.`plataforma`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_rolu`.`plataforma` (
  `id_plataforma` INT NOT NULL AUTO_INCREMENT,
  `nombre_plataforma` VARCHAR(500) NOT NULL,
  UNIQUE INDEX `id_plataforma_UNIQUE` (`id_plataforma` ASC) VISIBLE,
  PRIMARY KEY (`id_plataforma`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `proyecto_rolu`.`desarrolladora`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_rolu`.`desarrolladora` (
  `id_desarrolladora` INT NOT NULL AUTO_INCREMENT,
  `nombre_desarrolladora` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`id_desarrolladora`),
  UNIQUE INDEX `id_desarrolladora_UNIQUE` (`id_desarrolladora` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `proyecto_rolu`.`videojuego`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_rolu`.`videojuego` (
  `id_videojuego` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(500) NOT NULL,
  `fecha_lanzamiento` DATE NOT NULL,
  `nota` INT NOT NULL,
  `desarrolladora_id_desarrolladora` INT NOT NULL,
  PRIMARY KEY (`id_videojuego`),
  UNIQUE INDEX `id_videojuego_UNIQUE` (`id_videojuego` ASC) VISIBLE,
  INDEX `fk_videojuego_desarrolladora1_idx` (`desarrolladora_id_desarrolladora` ASC) VISIBLE,
  CONSTRAINT `fk_videojuego_desarrolladora1`
    FOREIGN KEY (`desarrolladora_id_desarrolladora`)
    REFERENCES `proyecto_rolu`.`desarrolladora` (`id_desarrolladora`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `proyecto_rolu`.`plataforma_disponible_videojuego`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_rolu`.`plataforma_disponible_videojuego` (
  `plataforma_id_plataforma` INT NOT NULL,
  `videojuego_id_videojuego` INT NOT NULL,
  PRIMARY KEY (`plataforma_id_plataforma`, `videojuego_id_videojuego`),
  INDEX `fk_plataforma_has_videojuego_videojuego1_idx` (`videojuego_id_videojuego` ASC) VISIBLE,
  INDEX `fk_plataforma_has_videojuego_plataforma_idx` (`plataforma_id_plataforma` ASC) VISIBLE,
  CONSTRAINT `fk_plataforma_has_videojuego_plataforma`
    FOREIGN KEY (`plataforma_id_plataforma`)
    REFERENCES `proyecto_rolu`.`plataforma` (`id_plataforma`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_plataforma_has_videojuego_videojuego1`
    FOREIGN KEY (`videojuego_id_videojuego`)
    REFERENCES `proyecto_rolu`.`videojuego` (`id_videojuego`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `proyecto_rolu`.`idioma`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_rolu`.`idioma` (
  `id_idioma` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`id_idioma`),
  UNIQUE INDEX `id_idioma_UNIQUE` (`id_idioma` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `proyecto_rolu`.`idioma_tiene_videojuego`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_rolu`.`idioma_tiene_videojuego` (
  `idioma_id_idioma` INT NOT NULL,
  `videojuego_id_videojuego` INT NOT NULL,
  PRIMARY KEY (`idioma_id_idioma`, `videojuego_id_videojuego`),
  INDEX `fk_idioma_has_videojuego_videojuego1_idx` (`videojuego_id_videojuego` ASC) VISIBLE,
  INDEX `fk_idioma_has_videojuego_idioma1_idx` (`idioma_id_idioma` ASC) VISIBLE,
  CONSTRAINT `fk_idioma_has_videojuego_idioma1`
    FOREIGN KEY (`idioma_id_idioma`)
    REFERENCES `proyecto_rolu`.`idioma` (`id_idioma`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_idioma_has_videojuego_videojuego1`
    FOREIGN KEY (`videojuego_id_videojuego`)
    REFERENCES `proyecto_rolu`.`videojuego` (`id_videojuego`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `proyecto_rolu`.`discografia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_rolu`.`discografia` (
  `id_discografia` INT NOT NULL AUTO_INCREMENT,
  `nombre_discografia` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`id_discografia`),
  UNIQUE INDEX `id_discografia_UNIQUE` (`id_discografia` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `proyecto_rolu`.`ost`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_rolu`.`ost` (
  `id_ost` INT NOT NULL AUTO_INCREMENT,
  `fecha` DATE NOT NULL,
  `nombre` VARCHAR(500) NOT NULL,
  `discografia_id_discografia` INT NOT NULL,
  PRIMARY KEY (`id_ost`),
  UNIQUE INDEX `id_ost_UNIQUE` (`id_ost` ASC) VISIBLE,
  INDEX `fk_ost_discografia1_idx` (`discografia_id_discografia` ASC) VISIBLE,
  CONSTRAINT `fk_ost_discografia1`
    FOREIGN KEY (`discografia_id_discografia`)
    REFERENCES `proyecto_rolu`.`discografia` (`id_discografia`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `proyecto_rolu`.`videojuego_tiene_ost`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_rolu`.`videojuego_tiene_ost` (
  `videojuego_id_videojuego` INT NOT NULL,
  `ost_id_ost` INT NOT NULL,
  PRIMARY KEY (`videojuego_id_videojuego`, `ost_id_ost`),
  INDEX `fk_videojuego_has_ost_ost1_idx` (`ost_id_ost` ASC) VISIBLE,
  INDEX `fk_videojuego_has_ost_videojuego1_idx` (`videojuego_id_videojuego` ASC) VISIBLE,
  CONSTRAINT `fk_videojuego_has_ost_videojuego1`
    FOREIGN KEY (`videojuego_id_videojuego`)
    REFERENCES `proyecto_rolu`.`videojuego` (`id_videojuego`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_videojuego_has_ost_ost1`
    FOREIGN KEY (`ost_id_ost`)
    REFERENCES `proyecto_rolu`.`ost` (`id_ost`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `proyecto_rolu`.`interprete`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_rolu`.`interprete` (
  `id_interprete` INT NOT NULL AUTO_INCREMENT,
  `nombre_artista` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`id_interprete`),
  UNIQUE INDEX `id_interprete_UNIQUE` (`id_interprete` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `proyecto_rolu`.`interprete_interpreta_ost`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_rolu`.`interprete_interpreta_ost` (
  `interprete_id_interprete` INT NOT NULL,
  `ost_id_ost` INT NOT NULL,
  PRIMARY KEY (`interprete_id_interprete`, `ost_id_ost`),
  INDEX `fk_interprete_has_ost_ost1_idx` (`ost_id_ost` ASC) VISIBLE,
  INDEX `fk_interprete_has_ost_interprete1_idx` (`interprete_id_interprete` ASC) VISIBLE,
  CONSTRAINT `fk_interprete_has_ost_interprete1`
    FOREIGN KEY (`interprete_id_interprete`)
    REFERENCES `proyecto_rolu`.`interprete` (`id_interprete`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_interprete_has_ost_ost1`
    FOREIGN KEY (`ost_id_ost`)
    REFERENCES `proyecto_rolu`.`ost` (`id_ost`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
