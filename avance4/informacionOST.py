import tkinter as tk
from tkinter import ttk
from tkcalendar import DateEntry


class informacionOST:
    def __init__(self, root, db):
        self.db = db
        self.data = []

        #Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('360x600')
        self.root.title("Informacion de OST")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.__config_treeview_informacionOST()

    def __config_treeview_informacionOST(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "OST")
        self.treeview.heading("#2", text = "artista")
        self.treeview.heading("#3", text = "Discografia")
        self.treeview.column("#0", minwidth = 40, width = 40, stretch = False)
        self.treeview.column("#1", minwidth = 120, width = 120, stretch = False)
        self.treeview.column("#2", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#3", minwidth = 100, width = 100, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 600, width = 360)
        self.llenar_treeview_informacionOST()
        self.root.after(0, self.llenar_treeview_informacionOST)


    def llenar_treeview_informacionOST(self):
        sql = """select *from view_informacionost"""
        data = self.db.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], i[2], i[3]))
            self.data = data

  
