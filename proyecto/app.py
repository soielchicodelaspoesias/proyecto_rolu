import tkinter as tk
import sys
from tkinter import Menu
from tkinter import LabelFrame, Label, Frame
from tkinter import Button
from PIL import Image, ImageTk

#
from database import Database
from idioma import idioma
from plataforma import plataforma
from discografia import discografia
from interprete import interprete
from videojuego import videojuego
from desarrolladora import desarrolladora
from ost import ost
from plataforma_disponible_videojuego import plataformadisponible
from videojuego_tiene_ost import videoOST
from interprete_interpreta import interpreta
from idioma_tiene_videojuego import idiomaVideojuego
from disponibilidad import disponibilidad
from informacionOST import informacionOST
from graficos import graficos
from discografia_ost import discografia_ost
from videojuego_por_desarrolladora import videojuego_por_desarrolladora
from videojuegos_ingresados import videojuegosingresados
from videojuegos_eliminados import videojuegoseliminados

class App:

    def __init__(self, db):
        self.db = db

        # Main window

        self.root = tk.Toplevel()

        # Algunas especificaciones de tamaño y título de la ventana
        self.root.geometry("900x600")
        self.root.title("ROLU APP")

        #
        self.__agrega_imagen_principal()
        self.__crea_botones_principales()

    def __agrega_imagen_principal(self):
        #
        frame = LabelFrame(self.root, text="", relief=tk.FLAT)
        frame.place(x=0, y=0, relwidth=1, relheight=1)

        image = Image.open("foto.png")
        photo = ImageTk.PhotoImage(image.resize((900, 600), Image.ANTIALIAS))
        label = Label(frame, image=photo)
        label.image = photo
        label.pack()

    # botones principales.
    def __crea_botones_principales(self):
        padx = 3
        pady = 3

        #
        b1 = Button(self.root, text="Plataforma", width=20)
        b1.grid(row=0, column=0, padx=padx, pady=pady)
        b1.bind('<Button-1>', self.__mostrar_plataforma)

        #
        b2 = Button(self.root, text="Idioma", width=20)
        b2.grid(row=1, column=0, padx=padx, pady=pady)
        b2.bind('<Button-1>', self.__mostrar_idioma)

        #
        b3 = Button(self.root, text="Discografia", width=20)
        b3.grid(row=2, column=0, padx=padx, pady=pady)
        b3.bind('<Button-1>', self.__mostrar_discografia)

        b4 = Button(self.root, text="OST", width=20)
        b4.grid(row=3, column=0, padx=padx, pady=pady)
        b4.bind('<Button-1>', self.__mostrar_ost)

        b5 = Button(self.root, text="Interprete", width=20)
        b5.grid(row=4, column=0, padx=padx, pady=pady)
        b5.bind('<Button-1>', self.__mostrar_interprete)

        b6 = Button(self.root, text="Desarrolladora", width=20)
        b6.grid(row=5, column=0, padx=padx, pady=pady)
        b6.bind('<Button-1>', self.__mostrar_desarolladora)

        b7 = Button(self.root, text="Videojuego", width=20)
        b7.grid(row=6, column=0, padx=padx, pady=pady)
        b7.bind('<Button-1>', self.__mostrar_videojuego)

        b8 = Button(self.root, text="Plataforma Videojuego", width=20)
        b8.grid(row=7, column=0, padx=padx, pady=pady)
        b8.bind('<Button-1>', self.__mostrar_plataformadisponible)

        b9 = Button(self.root, text="OST Videojuego", width=20)
        b9.grid(row=8, column=0, padx=padx, pady=pady)
        b9.bind('<Button-1>', self.__mostrar_videoOST)

        b10 = Button(self.root, text="OST interprete", width=20)
        b10.grid(row=9, column=0, padx=padx, pady=pady)
        b10.bind('<Button-1>', self.__mostrar_interpreta)

        b11 = Button(self.root, text="Idioma videojuego", width=20)
        b11.grid(row=10, column=0, padx=padx, pady=pady)
        b11.bind('<Button-1>', self.__mostrar_idiomaVideojuego)

        b12 = Button(self.root, text="Videojuegos insertados", width=20)
        b12.grid(row=11, column=0, padx=padx, pady=pady)
        b12.bind('<Button-1>', self.__mostrar_videojuegosingresados)

        b13 = Button(self.root, text="Videojuegos eliminados", width=20)
        b13.grid(row=12, column=0, padx=padx, pady=pady)
        b13.bind('<Button-1>', self.__mostrar_videojuegoseliminados)



    # muestra ventana equipos.
    def __mostrar_equipos(self):
        equipo(self.root, self.db)

    # muestra ventana jugadores.
    def __mostrar_plataforma(self, button):
        plataforma(self.root, self.db)

    def __mostrar_idioma(self, button):
        idioma(self.root, self.db)

    def __mostrar_discografia(self, button):
        discografia(self.root, self.db)

    def __mostrar_ost(self, button):
        ost(self.root, self.db)

    def __mostrar_interprete(self, button):
        interprete(self.root, self.db)

    def __mostrar_desarolladora(self, button):
        desarrolladora(self.root, self.db)

    def __mostrar_videojuego(self, button):
        videojuego(self.root, self.db)

    def __mostrar_plataformadisponible(self, button):
        plataformadisponible(self.root, self.db)

    def __mostrar_videoOST(self, button):
        videoOST(self.root, self.db)

    def __mostrar_interpreta(self, button):
        interpreta(self.root, self.db)

    def __mostrar_idiomaVideojuego(self, button):
        idiomaVideojuego(self.root, self.db)

    def __mostrar_videojuegosingresados(self, button):
        videojuegosingresados(self.root, self.db)

    def __mostrar_videojuegoseliminados(self, button):
        videojuegoseliminados(self.root, self.db)


class Username:

    def __init__(self, db):
        self.db = db


        self.root = tk.Toplevel()

        # Algunas especificaciones de tamaño y título de la ventana
        self.root.geometry("900x600")
        self.root.title("ROLU APP")

        #
        self.__agrega_imagen_principal()
        self.__crea_botones_principales()





    def __agrega_imagen_principal(self):
        #
        frame = LabelFrame(self.root, text="", relief=tk.FLAT)
        frame.place(x=0, y=0, relwidth=1, relheight=1)

        image = Image.open("foto.png")
        photo = ImageTk.PhotoImage(image.resize((900, 600), Image.ANTIALIAS))
        label = Label(frame, image=photo)
        label.image = photo
        label.pack()

    # botones principales.
    def __crea_botones_principales(self):
        padx = 3
        pady = 3

        frame = LabelFrame(self.root, text="Consultas", relief=tk.GROOVE)
        frame.place(x=0, y=10, width=212, relheight=0.85)

        frame1 = LabelFrame(self.root, text="Graficos", relief=tk.GROOVE)
        frame1.place(x=0, y=150, width=212, relheight=0.85)

        b1 = Button(frame, text="Disponibilidad", width=22)
        b1.grid(row=0, column=0, padx=padx, pady=pady)
        b1.bind('<Button-1>', self.__mostrar_disponibilidad)
        
        b2 = Button(frame, text="Informacion OST", width=22)
        b2.grid(row=1, column=0, padx=padx, pady=pady)
        b2.bind('<Button-1>', self.__mostrar_informacionOST)

        b3 = Button(frame, text="Consulta por desarrolladora", width=22)
        b3.grid(row=2, column=0, padx=padx, pady=pady)
        b3.bind('<Button-1>', self.__videojuego_por_desarrolladora)

        b4 = Button(frame1, text="Videojuego por desarrolladora", width=22)
        b4.grid(row=0, column=0, padx=padx, pady=pady)
        b4.bind('<Button-1>', self.__graficos)

        b5 = Button(frame1, text="Ost por discografia", width=22)
        b5.grid(row=1, column=0, padx=padx, pady=pady)
        b5.bind('<Button-1>', self.__discografia_ost)




    def __mostrar_disponibilidad(self, button):
        disponibilidad(self.root, self.db)

    def __mostrar_informacionOST(self, button):
        informacionOST(self.root, self.db)

    def __graficos(self, button):
        graficos(self.root, self.db)


    def __discografia_ost(self, button):
        discografia_ost(self.root, self.db)

    def __videojuego_por_desarrolladora(self, button):
        videojuego_por_desarrolladora(self.root, self.db)





def main():
    # Conecta a la base de datos
    db = Database()

    def command1(event):
        if entry1.get() == "admin" and entry2.get() == "admin":
            App(db)
        elif entry1.get() == "user" and entry2.get() == "user":
            Username(db)



    def command2():
        top.destroy()
        root.destroy()
        sys.exit()


    root = tk.Tk()
    top = tk.Toplevel()

    top.geometry("600x600")
    top.title('Login Screen')
    top.configure(background = "white")
    photo2 = tk.PhotoImage(file = "admin.gif")

    photo = tk.Label(top, image = photo2, bg = "white")

    label1 = tk.Label(top, text = "Username", font = ("helevetica", 15))
    label1.configure(background = "white")
    entry1 = tk.Entry(top)

    label2 = tk.Label(top, text = "Password", font = ("helevetica", 15))
    label2.configure(background = "white")
    entry2 = tk.Entry(top, show="*")
    button2 = tk.Button(top, text = "Cancel", font = ("helevetica", 15), command=lambda:command2())

    entry2.bind("<Return>", command1)


    label = tk.Label(top, text="", font = ("Arial", 9))
    label.configure(background = "white")
    photo.pack()
    label1.pack()
    entry1.pack()
    label2.pack()
    entry2.pack()
    button2.pack()
    label.pack()
    root.withdraw()
    root.mainloop()

if __name__ == "__main__":
    main()
