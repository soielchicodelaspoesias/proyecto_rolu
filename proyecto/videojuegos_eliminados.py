import tkinter as tk
from tkinter import ttk
from tkcalendar import DateEntry


class videojuegoseliminados:
    def __init__(self, root, db):
        self.db = db
        self.data = []

        #Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('750x600')
        self.root.title("Videojuegos eliminados")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.__config_treeview_videojuegoseliminados()

    def __config_treeview_videojuegoseliminados(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.heading("#2", text = "Fecha eliminado")
        self.treeview.column("#0", minwidth = 40, width = 40, stretch = False)
        self.treeview.column("#1", minwidth = 120, width = 120, stretch = False)
        self.treeview.column("#2", minwidth = 140, width = 140, stretch = False)
        
        self.treeview.place(x = 0, y = 0, height = 600, width = 750)
        self.llenar_treeview_videojuegoseliminados()
        self.root.after(0, self.llenar_treeview_videojuegoseliminados)


    def llenar_treeview_videojuegoseliminados(self):
        sql = """select *from videojuegos_eliminados;"""
        data = self.db.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], str(i[2])))
            self.data = data
