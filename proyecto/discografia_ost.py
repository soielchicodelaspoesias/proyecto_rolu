from tkinter import *
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg)
class discografia_ost:
    def __init__(self, root, db):
        self.root = root
        self.db = db
        self.graph = Toplevel()
        fig, ax = plt.subplots()
        x, y = self.__get_data()
        ax.set_title("Videojuegos por desarrolladora")
        ax.set_ylabel("Videojuegos")
        ax.set_xlabel("Desarolladora")
        ax.bar(x, y, color = "greenyellow")
        canvas = FigureCanvasTkAgg(fig, master = self.graph)
        canvas.draw()
        canvas.get_tk_widget().pack()
    def __get_data(self):
        sql = """select discografia.nombre_discografia, count(ost.nombre) from discografia join ost on ost.discografia_id_discografia = discografia.id_discografia group by discografia.nombre_discografia;"""
        data = self.db.run_select(sql)
        x = [i[0] for i in data]
        y = [i[1] for i in data]
        return x, y
