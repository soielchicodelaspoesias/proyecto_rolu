import tkinter as tk
from tkinter import ttk


class videojuego_por_desarrolladora:
    def __init__(self, root, db):
        self.db = db
        self.data = []

        self.root = tk.Toplevel()
        #Toplevel es una ventana que está un nivel arriba que la principal
        self.root.geometry('300x270')
        self.root.title("Videojuego por desarrolladora")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        self.__config_label()
        self.__config_entry()
        self.__config_button()

        self.root.mainloop()

    def __config_label(self):
            tk.Label(self.root, text = "Desarolladora: ").place(x = 5, y = 35, width = 140, height = 20)

    def __config_entry(self):
        self.combo = ttk.Combobox(self.root)
        self.combo.place(x = 120, y = 35, width = 150, height= 20)
        self.combo["values"], self.ids = self.fill_combo()

    def fill_combo(self): #
        sql = "select * from desarrolladora"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __config_button(self):
        tk.Button(self.root, text = "Consultar",
            command = self.consulta).place(x=100, y =230, width = 80, height = 20)

    def consulta(self):
        sql = """create or replace view view_videojuego_por_desarrolladora as select id_videojuego, nombre, fecha_lanzamiento, nota, desarrolladora.nombre_desarrolladora from videojuego join desarrolladora
        on videojuego.desarrolladora_id_desarrolladora = desarrolladora.id_desarrolladora
        where desarrolladora.id_desarrolladora = %(id_desarrolladora)s"""

        self.db.run_sql(sql, {"id_desarrolladora" : self.ids[self.combo.current()]})
        videojuego(self.db, self.ids[self.combo.current()])

class videojuego:
    def __init__(self, db, videjuego):
        self.db = db
        self.data = []
        self.videojuego = videjuego

        self.root = tk.Toplevel()

        self.__config_window()
        self.__config_treeview_videojuego_por_desarrolladora()

    def __config_window(self):
        self.root.geometry('500x300')
        self.root.title(' Videojuego por Desarolladora')
        self.root.resizable(width = 0, height = 0)


    def __config_treeview_videojuego_por_desarrolladora(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3", "#4"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.heading("#2", text = "Fecha")
        self.treeview.heading("#3", text = "Nota")
        self.treeview.heading("#4", text = "Desarrolladora")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#2", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#3", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#4", minwidth = 100, width = 100, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 600, width = 750)
        self.llenar_treeview_videojuego_por_desarrolladora()
        self.root.after(0, self.llenar_treeview_videojuego_por_desarrolladora)

    def llenar_treeview_videojuego_por_desarrolladora(self):
        sql = """ select * from view_videojuego_por_desarrolladora"""

        data = self.db.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], str(i[2]), i[3], i[4]))
            self.data = data
