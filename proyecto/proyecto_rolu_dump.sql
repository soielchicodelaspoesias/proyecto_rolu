-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: localhost    Database: proyecto_rolu
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `desarrolladora`
--

DROP TABLE IF EXISTS `desarrolladora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `desarrolladora` (
  `id_desarrolladora` int NOT NULL AUTO_INCREMENT,
  `nombre_desarrolladora` varchar(500) NOT NULL,
  PRIMARY KEY (`id_desarrolladora`),
  UNIQUE KEY `id_desarrolladora_UNIQUE` (`id_desarrolladora`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `desarrolladora`
--

LOCK TABLES `desarrolladora` WRITE;
/*!40000 ALTER TABLE `desarrolladora` DISABLE KEYS */;
INSERT INTO `desarrolladora` VALUES (1,'Rockstar'),(3,'EA'),(8,'Square Enix');
/*!40000 ALTER TABLE `desarrolladora` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discografia`
--

DROP TABLE IF EXISTS `discografia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `discografia` (
  `id_discografia` int NOT NULL AUTO_INCREMENT,
  `nombre_discografia` varchar(500) NOT NULL,
  PRIMARY KEY (`id_discografia`),
  UNIQUE KEY `id_discografia_UNIQUE` (`id_discografia`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discografia`
--

LOCK TABLES `discografia` WRITE;
/*!40000 ALTER TABLE `discografia` DISABLE KEYS */;
INSERT INTO `discografia` VALUES (1,'Chile records'),(2,'Empire'),(3,'New order');
/*!40000 ALTER TABLE `discografia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `idioma`
--

DROP TABLE IF EXISTS `idioma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `idioma` (
  `id_idioma` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) NOT NULL,
  PRIMARY KEY (`id_idioma`),
  UNIQUE KEY `id_idioma_UNIQUE` (`id_idioma`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `idioma`
--

LOCK TABLES `idioma` WRITE;
/*!40000 ALTER TABLE `idioma` DISABLE KEYS */;
INSERT INTO `idioma` VALUES (1,'Ingles'),(2,'Frances'),(6,'Ruso');
/*!40000 ALTER TABLE `idioma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `idioma_tiene_videojuego`
--

DROP TABLE IF EXISTS `idioma_tiene_videojuego`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `idioma_tiene_videojuego` (
  `idioma_id_idioma` int NOT NULL,
  `videojuego_id_videojuego` int NOT NULL,
  PRIMARY KEY (`idioma_id_idioma`,`videojuego_id_videojuego`),
  KEY `fk_idioma_has_videojuego_videojuego1_idx` (`videojuego_id_videojuego`),
  KEY `fk_idioma_has_videojuego_idioma1_idx` (`idioma_id_idioma`),
  CONSTRAINT `fk_idioma_has_videojuego_idioma1` FOREIGN KEY (`idioma_id_idioma`) REFERENCES `idioma` (`id_idioma`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_idioma_has_videojuego_videojuego1` FOREIGN KEY (`videojuego_id_videojuego`) REFERENCES `videojuego` (`id_videojuego`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `idioma_tiene_videojuego`
--

LOCK TABLES `idioma_tiene_videojuego` WRITE;
/*!40000 ALTER TABLE `idioma_tiene_videojuego` DISABLE KEYS */;
INSERT INTO `idioma_tiene_videojuego` VALUES (1,6),(6,6),(2,10);
/*!40000 ALTER TABLE `idioma_tiene_videojuego` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interprete`
--

DROP TABLE IF EXISTS `interprete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `interprete` (
  `id_interprete` int NOT NULL AUTO_INCREMENT,
  `nombre_artista` varchar(500) NOT NULL,
  PRIMARY KEY (`id_interprete`),
  UNIQUE KEY `id_interprete_UNIQUE` (`id_interprete`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interprete`
--

LOCK TABLES `interprete` WRITE;
/*!40000 ALTER TABLE `interprete` DISABLE KEYS */;
INSERT INTO `interprete` VALUES (1,'Michael Jackson'),(2,'Tylor Swift'),(5,'Bad Bunny');
/*!40000 ALTER TABLE `interprete` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interprete_interpreta_ost`
--

DROP TABLE IF EXISTS `interprete_interpreta_ost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `interprete_interpreta_ost` (
  `interprete_id_interprete` int NOT NULL,
  `ost_id_ost` int NOT NULL,
  PRIMARY KEY (`interprete_id_interprete`,`ost_id_ost`),
  KEY `fk_interprete_has_ost_ost1_idx` (`ost_id_ost`),
  KEY `fk_interprete_has_ost_interprete1_idx` (`interprete_id_interprete`),
  CONSTRAINT `fk_interprete_has_ost_interprete1` FOREIGN KEY (`interprete_id_interprete`) REFERENCES `interprete` (`id_interprete`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_interprete_has_ost_ost1` FOREIGN KEY (`ost_id_ost`) REFERENCES `ost` (`id_ost`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interprete_interpreta_ost`
--

LOCK TABLES `interprete_interpreta_ost` WRITE;
/*!40000 ALTER TABLE `interprete_interpreta_ost` DISABLE KEYS */;
INSERT INTO `interprete_interpreta_ost` VALUES (1,1),(2,1);
/*!40000 ALTER TABLE `interprete_interpreta_ost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ost`
--

DROP TABLE IF EXISTS `ost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ost` (
  `id_ost` int NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `nombre` varchar(500) NOT NULL,
  `discografia_id_discografia` int NOT NULL,
  PRIMARY KEY (`id_ost`),
  UNIQUE KEY `id_ost_UNIQUE` (`id_ost`),
  KEY `fk_ost_discografia1_idx` (`discografia_id_discografia`),
  CONSTRAINT `fk_ost_discografia1` FOREIGN KEY (`discografia_id_discografia`) REFERENCES `discografia` (`id_discografia`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ost`
--

LOCK TABLES `ost` WRITE;
/*!40000 ALTER TABLE `ost` DISABLE KEYS */;
INSERT INTO `ost` VALUES (1,'2021-12-12','Red SUN',1),(2,'2021-12-12','Stains of time',2),(4,'2021-12-12','Metal ',1),(5,'2021-12-13','Metal',1);
/*!40000 ALTER TABLE `ost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plataforma`
--

DROP TABLE IF EXISTS `plataforma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plataforma` (
  `id_plataforma` int NOT NULL AUTO_INCREMENT,
  `nombre_plataforma` varchar(500) NOT NULL,
  PRIMARY KEY (`id_plataforma`),
  UNIQUE KEY `id_plataforma_UNIQUE` (`id_plataforma`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plataforma`
--

LOCK TABLES `plataforma` WRITE;
/*!40000 ALTER TABLE `plataforma` DISABLE KEYS */;
INSERT INTO `plataforma` VALUES (1,'STEAM'),(2,'Origin'),(3,'Xbox');
/*!40000 ALTER TABLE `plataforma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plataforma_disponible_videojuego`
--

DROP TABLE IF EXISTS `plataforma_disponible_videojuego`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plataforma_disponible_videojuego` (
  `plataforma_id_plataforma` int NOT NULL,
  `videojuego_id_videojuego` int NOT NULL,
  PRIMARY KEY (`plataforma_id_plataforma`,`videojuego_id_videojuego`),
  KEY `fk_plataforma_has_videojuego_videojuego1_idx` (`videojuego_id_videojuego`),
  KEY `fk_plataforma_has_videojuego_plataforma_idx` (`plataforma_id_plataforma`),
  CONSTRAINT `fk_plataforma_has_videojuego_plataforma` FOREIGN KEY (`plataforma_id_plataforma`) REFERENCES `plataforma` (`id_plataforma`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_plataforma_has_videojuego_videojuego1` FOREIGN KEY (`videojuego_id_videojuego`) REFERENCES `videojuego` (`id_videojuego`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plataforma_disponible_videojuego`
--

LOCK TABLES `plataforma_disponible_videojuego` WRITE;
/*!40000 ALTER TABLE `plataforma_disponible_videojuego` DISABLE KEYS */;
INSERT INTO `plataforma_disponible_videojuego` VALUES (1,6),(1,10),(3,10);
/*!40000 ALTER TABLE `plataforma_disponible_videojuego` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videojuego`
--

DROP TABLE IF EXISTS `videojuego`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `videojuego` (
  `id_videojuego` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) NOT NULL,
  `fecha_lanzamiento` date NOT NULL,
  `nota` int NOT NULL,
  `desarrolladora_id_desarrolladora` int NOT NULL,
  PRIMARY KEY (`id_videojuego`),
  UNIQUE KEY `id_videojuego_UNIQUE` (`id_videojuego`),
  KEY `fk_videojuego_desarrolladora1_idx` (`desarrolladora_id_desarrolladora`),
  CONSTRAINT `fk_videojuego_desarrolladora1` FOREIGN KEY (`desarrolladora_id_desarrolladora`) REFERENCES `desarrolladora` (`id_desarrolladora`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videojuego`
--

LOCK TABLES `videojuego` WRITE;
/*!40000 ALTER TABLE `videojuego` DISABLE KEYS */;
INSERT INTO `videojuego` VALUES (6,'Battlefield V','2018-11-13',7,3),(10,'Grand Theft Auto V','2018-12-06',10,1),(11,'Final Fantasy VII','2019-12-18',9,8);
/*!40000 ALTER TABLE `videojuego` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `videojuego_AFTER_INSERT` AFTER INSERT ON `videojuego` FOR EACH ROW BEGIN
insert into videojuegos_insertados (id_videojuego, nombre, fecha_ingresado)
VALUES (NEW.id_videojuego, NEW.nombre, curdate());
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `videojuego_BEFORE_DELETE` BEFORE DELETE ON `videojuego` FOR EACH ROW BEGIN
INSERT INTO videojuegos_eliminados
(id_videojuego, nombre, fecha_eliminado)
VALUES
(OLD.id_videojuego, OLD.nombre, curdate());
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `videojuego_tiene_ost`
--

DROP TABLE IF EXISTS `videojuego_tiene_ost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `videojuego_tiene_ost` (
  `videojuego_id_videojuego` int NOT NULL,
  `ost_id_ost` int NOT NULL,
  PRIMARY KEY (`videojuego_id_videojuego`,`ost_id_ost`),
  KEY `fk_videojuego_has_ost_ost1_idx` (`ost_id_ost`),
  KEY `fk_videojuego_has_ost_videojuego1_idx` (`videojuego_id_videojuego`),
  CONSTRAINT `fk_videojuego_has_ost_ost1` FOREIGN KEY (`ost_id_ost`) REFERENCES `ost` (`id_ost`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_videojuego_has_ost_videojuego1` FOREIGN KEY (`videojuego_id_videojuego`) REFERENCES `videojuego` (`id_videojuego`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videojuego_tiene_ost`
--

LOCK TABLES `videojuego_tiene_ost` WRITE;
/*!40000 ALTER TABLE `videojuego_tiene_ost` DISABLE KEYS */;
INSERT INTO `videojuego_tiene_ost` VALUES (6,1),(6,2),(11,2);
/*!40000 ALTER TABLE `videojuego_tiene_ost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videojuegos_eliminados`
--

DROP TABLE IF EXISTS `videojuegos_eliminados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `videojuegos_eliminados` (
  `id_videojuego` int DEFAULT NULL,
  `nombre` varchar(500) DEFAULT NULL,
  `fecha_eliminado` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videojuegos_eliminados`
--

LOCK TABLES `videojuegos_eliminados` WRITE;
/*!40000 ALTER TABLE `videojuegos_eliminados` DISABLE KEYS */;
INSERT INTO `videojuegos_eliminados` VALUES (1,'Grand Theft Auto','2021-12-12'),(7,'Battlefield V','2021-12-12'),(8,'Battlefield V','2021-12-13'),(9,'Grand Theft Auto IV','2021-12-13'),(12,'Final Fantasy VII','2021-12-13'),(12,'Final Fantasy VII','2021-12-13');
/*!40000 ALTER TABLE `videojuegos_eliminados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videojuegos_insertados`
--

DROP TABLE IF EXISTS `videojuegos_insertados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `videojuegos_insertados` (
  `id_videojuego` int DEFAULT NULL,
  `nombre` varchar(500) DEFAULT NULL,
  `fecha_ingresado` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videojuegos_insertados`
--

LOCK TABLES `videojuegos_insertados` WRITE;
/*!40000 ALTER TABLE `videojuegos_insertados` DISABLE KEYS */;
INSERT INTO `videojuegos_insertados` VALUES (4,'Final Fantasy VII','2021-12-12'),(5,'The Bindinf of Isaac','2021-12-12'),(6,'Battlefield V','2021-12-12'),(7,'Battlefield V','2021-12-12'),(8,'Battlefield V','2021-12-13'),(9,'Grand Theft Auto V','2021-12-13'),(10,'Grand Theft Auto V','2021-12-13'),(11,'Final Fantasy VII','2021-12-13'),(12,'Final Fantasy VII','2021-12-13'),(12,'Final Fantasy VII','2021-12-13');
/*!40000 ALTER TABLE `videojuegos_insertados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `view_disponibilidad`
--

DROP TABLE IF EXISTS `view_disponibilidad`;
/*!50001 DROP VIEW IF EXISTS `view_disponibilidad`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_disponibilidad` AS SELECT 
 1 AS `id_videojuego`,
 1 AS `nombre`,
 1 AS `fecha_lanzamiento`,
 1 AS `nota`,
 1 AS `nombre_desarrolladora`,
 1 AS `nombre_plataforma`,
 1 AS `idioma`,
 1 AS `ost`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_informacionost`
--

DROP TABLE IF EXISTS `view_informacionost`;
/*!50001 DROP VIEW IF EXISTS `view_informacionost`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `view_informacionost` AS SELECT 
 1 AS `id_ost`,
 1 AS `OST`,
 1 AS `nombre_artista`,
 1 AS `nombre_discografia`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'proyecto_rolu'
--
/*!50003 DROP PROCEDURE IF EXISTS `eliminar_duplicados` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_duplicados`()
BEGIN WITH cte AS ( SELECT id_videojuego, ROW_NUMBER() OVER (PARTITION BY nombre ORDER BY nombre) AS rn FROM videojuego ) DELETE FROM videojuego USING
 videojuego JOIN cte ON videojuego.id_videojuego = cte.id_videojuego WHERE cte.rn > 1; END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `eliminar_duplicados_desarrolladora` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_duplicados_desarrolladora`()
BEGIN WITH cte6 AS ( SELECT id_desarrolladora, ROW_NUMBER() OVER (PARTITION BY nombre_desarrolladora ORDER BY nombre_desarrolladora) AS rn6 FROM desarrolladora ) DELETE FROM desarrolladora USING desarrolladora JOIN cte6 ON desarrolladora.id_desarrolladora = cte6.id_desarrolladora WHERE cte6.rn6 > 1; END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `eliminar_duplicados_discografia` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_duplicados_discografia`()
BEGIN WITH cte3 AS ( SELECT id_discografia, ROW_NUMBER() OVER (PARTITION BY nombre_discografia ORDER BY nombre_discografia) AS rn3 FROM discografia ) DELETE FROM discografia USING discografia JOIN cte3 ON discografia.id_discografia = cte3.id_discografia WHERE cte3.rn3 > 1; END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `eliminar_duplicados_disponibilidad` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_duplicados_disponibilidad`()
BEGIN WITH cte7 AS ( SELECT idioma, ROW_NUMBER() OVER (PARTITION BY nombre ORDER BY nombre) AS rn7 FROM view_disponibilidad ) DELETE FROM view_disponibilidad USING view_disponibilidad JOIN cte7 ON view_disponibilidad.idioma = cte7.idioma WHERE cte7.rn7 > 1; END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `eliminar_duplicados_idioma` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_duplicados_idioma`()
BEGIN WITH cte2 AS ( SELECT id_idioma, ROW_NUMBER() OVER (PARTITION BY nombre ORDER BY nombre) AS rn2 FROM idioma ) DELETE FROM idioma USING idioma JOIN cte2 ON idioma.id_idioma = cte2.id_idioma WHERE cte2.rn2 > 1; END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `eliminar_duplicados_interprete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_duplicados_interprete`()
BEGIN WITH cte5 AS ( SELECT id_interprete, ROW_NUMBER() OVER (PARTITION BY nombre_artista ORDER BY nombre_artista) AS rn5 FROM interprete ) DELETE FROM interprete USING interprete JOIN cte5 ON interprete.id_interprete = cte5.id_interprete WHERE cte5.rn5 > 1; END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `eliminar_duplicados_ost` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_duplicados_ost`()
BEGIN WITH cte4 AS ( SELECT id_ost, ROW_NUMBER() OVER (PARTITION BY nombre ORDER BY nombre) AS rn4 FROM ost ) DELETE FROM ost USING ost JOIN cte4 ON ost.id_ost = cte4.id_ost WHERE cte4.rn4 > 1; END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `eliminar_duplicados_plataforma` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_duplicados_plataforma`()
BEGIN WITH cte1 AS ( SELECT id_plataforma, ROW_NUMBER() OVER (PARTITION BY nombre_plataforma ORDER BY nombre_plataforma) AS rn1 FROM plataforma ) DELETE FROM plataforma USING plataforma JOIN cte1 ON plataforma.id_plataforma = cte1.id_plataforma WHERE cte1.rn1 > 1; END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `view_disponibilidad`
--

/*!50001 DROP VIEW IF EXISTS `view_disponibilidad`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_disponibilidad` AS select `videojuego`.`id_videojuego` AS `id_videojuego`,`videojuego`.`nombre` AS `nombre`,`videojuego`.`fecha_lanzamiento` AS `fecha_lanzamiento`,`videojuego`.`nota` AS `nota`,`desarrolladora`.`nombre_desarrolladora` AS `nombre_desarrolladora`,`plataforma`.`nombre_plataforma` AS `nombre_plataforma`,`idioma`.`nombre` AS `idioma`,`ost`.`nombre` AS `ost` from (((((((`videojuego` join `desarrolladora` on((`videojuego`.`desarrolladora_id_desarrolladora` = `desarrolladora`.`id_desarrolladora`))) join `plataforma_disponible_videojuego` on((`plataforma_disponible_videojuego`.`videojuego_id_videojuego` = `videojuego`.`id_videojuego`))) join `plataforma` on((`plataforma_disponible_videojuego`.`plataforma_id_plataforma` = `plataforma`.`id_plataforma`))) join `idioma_tiene_videojuego`) join `idioma` on(((`idioma_tiene_videojuego`.`idioma_id_idioma` = `idioma`.`id_idioma`) and (`idioma_tiene_videojuego`.`idioma_id_idioma` = `idioma`.`id_idioma`)))) join `videojuego_tiene_ost`) join `ost` on(((`videojuego_tiene_ost`.`ost_id_ost` = `ost`.`id_ost`) and (`videojuego_tiene_ost`.`ost_id_ost` = `ost`.`id_ost`)))) order by `videojuego`.`id_videojuego` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_informacionost`
--

/*!50001 DROP VIEW IF EXISTS `view_informacionost`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_informacionost` AS select `ost`.`id_ost` AS `id_ost`,`ost`.`nombre` AS `OST`,`interprete`.`nombre_artista` AS `nombre_artista`,`discografia`.`nombre_discografia` AS `nombre_discografia` from (((`ost` join `discografia` on((`ost`.`discografia_id_discografia` = `discografia`.`id_discografia`))) join `interprete_interpreta_ost` on((`interprete_interpreta_ost`.`ost_id_ost` = `ost`.`id_ost`))) join `interprete` on((`interprete`.`id_interprete` = `interprete_interpreta_ost`.`interprete_id_interprete`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-13 20:19:33
