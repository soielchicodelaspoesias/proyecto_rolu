import tkinter as tk
from tkinter import ttk
from tkcalendar import DateEntry


class disponibilidad:
    def __init__(self, root, db):
        self.db = db
        self.data = []

        #Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('750x600')
        self.root.title("disponibilidad")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.__config_treeview_disponibilidad()

    def __config_treeview_disponibilidad(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3", "#4", "#5", "#6", "#7"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.heading("#2", text = "Fecha")
        self.treeview.heading("#3", text = "Nota")
        self.treeview.heading("#4", text = "Desarrolladora")
        self.treeview.heading("#5", text = "Plataforma")
        self.treeview.heading("#6", text = "Idioma")
        self.treeview.heading("#7", text = "OST")
        self.treeview.column("#0", minwidth = 40, width = 40, stretch = False)
        self.treeview.column("#1", minwidth = 120, width = 120, stretch = False)
        self.treeview.column("#2", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#3", minwidth = 40, width = 40, stretch = False)
        self.treeview.column("#4", minwidth = 120, width = 120, stretch = False)
        self.treeview.column("#5", minwidth = 120, width = 120, stretch = False)
        self.treeview.column("#6", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#7", minwidth = 120, width = 120, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 600, width = 750)
        self.llenar_treeview_disponibilidad()
        self.root.after(0, self.llenar_treeview_disponibilidad)


    def llenar_treeview_disponibilidad(self):
        sql = """select id_videojuego, videojuego.nombre, fecha_lanzamiento, nota, desarrolladora.nombre_desarrolladora, plataforma.nombre_plataforma, idioma.nombre as idioma, ost.nombre as ost  from videojuego join desarrolladora on videojuego.desarrolladora_id_desarrolladora = desarrolladora.id_desarrolladora join plataforma_disponible_videojuego on plataforma_disponible_videojuego.videojuego_id_videojuego = videojuego.id_videojuego join plataforma on plataforma_disponible_videojuego.plataforma_id_plataforma = plataforma.id_plataforma join idioma_tiene_videojuego join idioma on idioma_tiene_videojuego.idioma_id_idioma= idioma.id_idioma and idioma_tiene_videojuego.idioma_id_idioma = idioma.id_idioma join videojuego_tiene_ost join ost on videojuego_tiene_ost.ost_id_ost = ost.id_ost and videojuego_tiene_ost.ost_id_ost = ost.id_ost order by id_videojuego;"""
        data = self.db.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], str(i[2]), i[3], i[4], i[5], i[6], i[7]))
            self.data = data
