# ROLU APP
Aplicacion Tkinter para almacenar Videojuegos. 


## Problematica:
``` 
Una distribuidora desea almacenar información sobre videojuegos con información básica sobre estos para su posterior orden y facilitar a las personas en la búsqueda de videojuegos con información específica.

Para esto necesita recopilar la información de la fecha de lanzamiento, nota, nombre, diseñadora, idioma, OST, discografía, intérprete y la plataforma en la cual el videojuego está disponible.

Para solucionar la problemática se requiere diseñar una base de datos con la información requerida por la distribuidora, primeramente se creará con un modelo MER para luego recrear el modelo con el programa MySQL Workbench.

Esta solución ayudará a la distribuidora a tener una base de datos con la información que requiere y además podrá acceder a ella de una forma más rápida.
``` 


## Requisitos:

 > Python 3
 
 > Mysql
 
 > Tkinter

## Librerías de Python 3:

 > mysql
 
 > matplotlib
 
 > PIL
 
 > tkinter

 > tkcalendar

 >askyesno

 >askquestion

## Instalación de paquetes

``` 
apt install mysql-server
pip install mysql-connector-python
pip install matplotlib
pip install PIL
sudo apt-get update
sudo apt install python3-tk
pip install tkcalendar
```

## Creación de la base de datos y usuarios:

``` 
 --intregresar a mysql
 #mysql -u root-p
 #create proyecto_rolu;
 --salir de mysql con Ctrl + 'd'
 --desde terminal, en el directorio, proyecto, donde se encuentra el archivo proyecto_rolu_dump.sql
 #mysql -u root -p proyecto_rolu < proyecto_rolu.sql
 --Ingresar nuevamente a mysql
 #mysql -u root -p
 #use proyecto_rolu;
 #create user 'proyecto_rolu'@'localhost' identified by 'password';
 #grant all privileges on proyecto_rolu.* to 'proyecto_rolu'@'localhost';
 #flush privileges;
``` 

## Ejecución: 

``` 
 #python3 app.py
 -- Se inicia con una ventana de login, para esta hay dos perfiles, la cuenta 'admin', cuya contraseña es 'admin',
 permite Insertar, modificar y eliminar en todas las tablas de la base de datos, mientras que el segundo perfil 
 es 'user' y la contraseña es 'user', aqui podra consultar todos los videojuegos creados, ademas de una busqueda 
 segun su desalloradora y dos histogramas, uno de cuantos juegos existen por desarrolladora y el segundo de OSTs por su discografia. 
```

### Autores:
 > Luis Rebolledo
 > Rodrigo Valenzuela

## Version:
 > 1.0

