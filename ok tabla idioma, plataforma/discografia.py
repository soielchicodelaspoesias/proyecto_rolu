import tkinter as tk
from tkinter import ttk

class discografia:
    def __init__(self, root, db):
        self.db = db
        self.data = []

        self.root = tk.Toplevel()
        self.root.geometry('600x400')
        self.root.title("discografias")
        self.root.resizable(width=0, height=0)

        # toplevel modal

        self.root.transient(root)

        #
        self.__config_treeview_discografia()
        self.__config_buttons_discografia()

    def __config_treeview_discografia(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Discografia")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 300, width = 300, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 700)
        self.llenar_treeview_discografia()
        self.root.after(0, self.llenar_treeview_discografia)

    def __config_buttons_discografia(self):
        tk.Button(self.root, text="Insertar discografia",
            command = self.__insertar_discografia).place(x = 0, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Modificar discografia",
            command = self.__modificar_discografia).place(x = 200, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Eliminar discografia",
            command = self.__eliminar_discografia).place(x = 400, y = 350, width = 200, height = 50)

    def llenar_treeview_discografia(self):
        sql = """select * from discografia;"""
        data = self.db.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1]), iid = i[0])
            self.data = data

    def __insertar_discografia(self):
        insertar_discografia(self.db, self)

    def __modificar_discografia(self):
        if(self.treeview.focus() != ""):
            sql = """select * from discografia where id_discografia = %(id_discografia)s;"""

            row_data = self.db.run_select_filter(sql, {"id_discografia": self.treeview.focus()})[0]
            modificar_discografia(self.db, self, row_data)

    def __eliminar_discografia(self):
        sql = "delete from discografia where id_discografia = %(id_discografia)s;"
        self.db.run_sql(sql, {"id_discografia": self.treeview.focus()})
        self.llenar_treeview_discografia()

class insertar_discografia:
    def __init__(self, db, padre):
        self.padre = padre
        self.db = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Insertar discografia")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):
        tk.Label(self.insert_datos, text = "Nombre: ").place(x = 10, y = 10, width = 80, height = 20)

    def __config_entry(self):
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 110, y = 10, width = 80, height = 20)

    def __config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =100, width = 200, height = 20)

    def __insertar(self): #Insercion en la base de datos.
        sql = """insert discografia (nombre_discografia)
            values (%(nombre_discografia)s);"""
        self.db.run_sql(sql, {"nombre_discografia": self.entry_nombre.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_discografia()

class modificar_discografia:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.db = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Modificar discografia")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self): #Labels
        tk.Label(self.insert_datos, text = "Nombre: ").place(x = 10, y = 10, width = 80, height = 20)

    def config_entry(self):#Se configuran los inputs
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 110, y = 10, width = 80, height = 20)
        self.entry_nombre.insert(0, self.row_data[1])

    def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.modificar).place(x=0, y =100, width = 200, height = 20)

    def modificar(self): #Insercion en la base de datos.
        sql = """update discografia set nombre_discografia = %(nombre_discografia)s where id_discografia = %(id_discografia)s;"""
        self.db.run_sql(sql, {"nombre_discografia": self.entry_nombre.get(), "id_discografia": int(self.row_data[0])})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_discografia()
