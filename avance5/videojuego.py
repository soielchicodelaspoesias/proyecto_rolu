import tkinter as tk
from tkinter import ttk
from tkcalendar import DateEntry
from tkinter.messagebox import askyesno, askquestion

class videojuego:
    def __init__(self, root, db):
        self.db = db
        self.data = []

        #Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('600x400')
        self.root.title("videojuego")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.__config_treeview_videojuego()
        self.__config_buttons_videojuego()

    def __config_treeview_videojuego(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3", "#4"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.heading("#2", text = "Fecha de lanzamiento")
        self.treeview.heading("#3", text = "Nota")
        self.treeview.heading("#4", text = "Desarrolladora")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 120, width = 120, stretch = False)
        self.treeview.column("#2", minwidth = 150, width = 150, stretch = False)
        self.treeview.column("#3", minwidth = 90, width = 90, stretch = False)
        self.treeview.column("#4", minwidth = 120, width = 120, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 700)
        self.llenar_treeview_videojuego()
        self.root.after(0, self.llenar_treeview_videojuego)

    def __config_buttons_videojuego(self):
        tk.Button(self.root, text="Insertar videojuego",
            command = self.__insertar_videojuego).place(x = 0, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Modificar videojuego",
            command = self.__modificar_videojuego).place(x = 200, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Eliminar videojuego",
            command = self.__eliminar_videojuego).place(x = 400, y = 350, width = 200, height = 50)



    def llenar_treeview_videojuego(self):
        sql2 = "call eliminar_duplicados();"
        self.db.runy_sql(sql2)
        sql = """select id_videojuego, nombre, fecha_lanzamiento, nota, desarrolladora.nombre_desarrolladora from videojuego join desarrolladora
        on videojuego.desarrolladora_id_desarrolladora = desarrolladora.id_desarrolladora;"""
        data = self.db.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], str(i[2]), i[3], i[4]), iid = i[0])
            self.data = data

    def __insertar_videojuego(self):
        insertar_videojuego(self.db, self)

    def __modificar_videojuego(self):
        if(self.treeview.focus() != ""):
            respuesta = askyesno(title = "Confirmacion", message = "Esta seguro de modificar?")
            if respuesta == True:
                sql = """select id_videojuego, nombre, fecha_lanzamiento, nota, desarrolladora.nombre_desarrolladora from videojuego
            join desarrolladora
            on videojuego.desarrolladora_id_desarrolladora = desarrolladora.id_desarrolladora where id_videojuego = %(id_videojuego)s;"""
                row_data = self.db.run_select_filter(sql, {"id_videojuego": self.treeview.focus()})[0]
                modificar_videojuego(self.db, self, row_data)

    def __eliminar_videojuego(self):
        respuesta = askyesno(title = "Confirmacion", message = "Esta seguro de eliminar?")
        if respuesta == True:
            sql = "delete from videojuego where id_videojuego = %(id_videojuego)s"
            self.db.run_sql(sql, {"id_videojuego": self.treeview.focus()})
            self.llenar_treeview_videojuego()


class insertar_videojuego:
    def __init__(self, db, padre):
        self.padre = padre
        self.db = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Insertar videojuego")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):
        tk.Label(self.insert_datos, text = "Nombre: ").place(x = 10, y = 10, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Fecha: ").place(x = 10, y = 30, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Nota: ").place(x = 10, y = 50, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Desarolladora: ").place(x = 10, y = 70, width = 80, height = 30)

    def __config_entry(self):
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 110, y = 10, width = 80, height = 20)
        self.combo = DateEntry(self.insert_datos,selectmode='day')
        self.combo.place(x = 110, y = 30, width = 80, height = 20)

        self.combo1 = ttk.Combobox(self.insert_datos)
        self.combo1.place(x = 110, y = 50, width = 80, height= 20)
        self.combo1["values"] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

        self.combo2 = ttk.Combobox(self.insert_datos)
        self.combo2.place(x = 110, y = 70, width = 80, height= 20)
        self.combo2["values"], self.ids = self.__fill_combo()

    def __config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =100, width = 200, height = 20)

    def __fill_combo(self): #
        sql = "select * from desarrolladora"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]


    def __insertar(self): #Insercion en la base de datos.
        sql = """insert videojuego (desarrolladora_id_desarrolladora, nombre, fecha_lanzamiento, nota)
            values (%(desarrolladora_id_desarrolladora)s, %(nombre)s, %(fecha_lanzamiento)s, %(nota)s)"""

        self.db.run_sql(sql, {"desarrolladora_id_desarrolladora": self.ids[self.combo2.current()],
                "nombre": self.entry_nombre.get(),
                "fecha_lanzamiento": self.combo.get_date(),
                "nota": self.combo1.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_videojuego()
class modificar_videojuego:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.db = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Modificar videojuego")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self): #Labels
        tk.Label(self.insert_datos, text = "Nombre: ").place(x = 10, y = 10, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Fecha: ").place(x = 10, y = 30, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Nota: ").place(x = 10, y = 50, width = 80, height = 30)
        tk.Label(self.insert_datos, text = "Desarolladora: ").place(x = 10, y = 70, width = 80, height = 30)

    def config_entry(self):#Se configuran los inputs
            self.entry_nombre = tk.Entry(self.insert_datos)
            self.entry_nombre.place(x = 110, y = 10, width = 80, height = 20)
            self.entry_nombre.insert(0, self.row_data[1])

            self.combo = DateEntry(self.insert_datos,selectmode='day')
            self.combo.place(x = 110, y = 30, width = 80, height = 20)
            self.combo.insert(0, self.row_data[2])

            self.combo1 = ttk.Combobox(self.insert_datos)
            self.combo1.place(x = 110, y = 50, width = 80, height= 20)
            self.combo1["values"] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
            self.combo1.insert(0, self.row_data[3])

            self.combo2 = ttk.Combobox(self.insert_datos)
            self.combo2.place(x = 110, y = 70, width = 80, height= 20)
            self.combo2["values"], self.ids = self.fill_combo()
            self.combo2.insert(0, self.row_data[4])


    def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.modificar).place(x=0, y =100, width = 200, height = 20)

    def modificar(self): #Insercion en la base de datos.
        sql = """update videojuego set
        nombre = %(nombre)s, fecha_lanzamiento = %(fecha_lanzamiento)s, nota = %(nota)s, desarrolladora_id_desarrolladora = %(desarrolladora_id_desarrolladora)s
        where id_videojuego = %(id_videojuego)s"""

        self.db.run_sql(sql, {"id_videojuego": int(self.row_data[0]),
            "nombre": self.entry_nombre.get(),
            "fecha_lanzamiento": self.combo.get_date(),
            "nota": self.combo1.get(),
            "desarrolladora_id_desarrolladora": self.ids[self.combo2.current()]})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_videojuego()

    def fill_combo(self): #
        sql = "select * from desarrolladora"
        self.data = self.db.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]
