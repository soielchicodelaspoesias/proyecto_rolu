from tkinter import *
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg)
class graficos:
    def __init__(self, root, db):
        self.root = root
        self.db = db
        self.graph = Toplevel()
        fig, ax = plt.subplots()
        x, y = self.__get_data()
        ax.set_title("Videojuegos por desarrolladora")
        ax.set_ylabel("Videojuegos")
        ax.set_xlabel("Desarolladora")
        ax.bar(x, y, color = "greenyellow")
        canvas = FigureCanvasTkAgg(fig, master = self.graph)
        canvas.draw()
        canvas.get_tk_widget().pack()
    def __get_data(self):
        sql = """select desarrolladora.nombre_desarrolladora, count(videojuego.nombre) from desarrolladora join videojuego on videojuego.desarrolladora_id_desarrolladora = desarrolladora.id_desarrolladora group by desarrolladora.nombre_desarrolladora"""
        data = self.db.run_select(sql)
        x = [i[0] for i in data]
        y = [i[1] for i in data]
        return x, y
