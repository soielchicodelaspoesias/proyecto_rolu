import tkinter as tk
from tkinter import ttk
from tkinter.messagebox import askyesno, askquestion

class interprete:
    def __init__(self, root, db):
        self.db = db
        self.data = []

        self.root = tk.Toplevel()
        self.root.geometry('600x400')
        self.root.title("interprete")
        self.root.resizable(width=0, height=0)

        # toplevel modal

        self.root.transient(root)

        #
        self.__config_treeview_interprete()
        self.__config_buttons_interprete()

    def __config_treeview_interprete(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "Interprete")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 500, width = 500, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 700)
        self.llenar_treeview_interprete()
        self.root.after(0, self.llenar_treeview_interprete)

    def __config_buttons_interprete(self):
        tk.Button(self.root, text="Insertar Interprete",
            command = self.__insertar_interprete).place(x = 0, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Modificar Interprete",
            command = self.__modificar_interprete).place(x = 200, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Eliminar Interprete",
            command = self.__eliminar_interprete).place(x = 400, y = 350, width = 200, height = 50)

    def llenar_treeview_interprete(self):
        sql2 = "call eliminar_duplicados_interprete();"
        self.db.runy_sql(sql2)
        sql = """select * from interprete;"""
        data = self.db.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], i[1]), iid = i[0])
            self.data = data

    def __insertar_interprete(self):
        insertar_interprete(self.db, self)

    def __modificar_interprete(self):
        if(self.treeview.focus() != ""):
            respuesta = askyesno(title = "Confirmacion", message = "Esta seguro de modificar?")
            if respuesta == True:
                sql = """select * from interprete where id_interprete = %(id_interprete)s;"""
                row_data = self.db.run_select_filter(sql, {"id_interprete": self.treeview.focus()})[0]
                modificar_interprete(self.db, self, row_data)

    def __eliminar_interprete(self):
        respuesta = askyesno(title = "Confirmacion", message = "Esta seguro de eliminar?")
        if respuesta == True:
            sql = "delete from interprete where id_interprete = %(id_interprete)s;"
            self.db.run_sql(sql, {"id_interprete": self.treeview.focus()})
            self.llenar_treeview_interprete()

class insertar_interprete:
    def __init__(self, db, padre):
        self.padre = padre
        self.db = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Insertar Interprete")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):
        tk.Label(self.insert_datos, text = "Nombre: ").place(x = 10, y = 10, width = 80, height = 20)

    def __config_entry(self):
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 110, y = 10, width = 80, height = 20)

    def __config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =100, width = 200, height = 20)

    def __insertar(self): #Insercion en la base de datos.
        sql = """insert into interprete (nombre_artista) values (%(nombre_artista)s);"""
        self.db.run_sql(sql, {"nombre_artista": self.entry_nombre.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_interprete()

class modificar_interprete:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.db = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Modificar interprete")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self): #Labels
        tk.Label(self.insert_datos, text = "Nombre: ").place(x = 10, y = 10, width = 80, height = 20)

    def config_entry(self):#Se configuran los inputs
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 110, y = 10, width = 80, height = 20)
        self.entry_nombre.insert(0, self.row_data[1])

    def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.modificar).place(x=0, y =100, width = 200, height = 20)

    def modificar(self): #Insercion en la base de datos.
        sql = """update interprete set nombre_artista = %(nombre_artista)s where id_interprete = %(id_interprete)s;"""
        self.db.run_sql(sql, {"nombre_artista": self.entry_nombre.get(), "id_interprete": int(self.row_data[0])})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_interprete()
